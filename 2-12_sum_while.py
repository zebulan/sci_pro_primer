from __future__ import division, print_function

# # infinite loop below because k never becomes greater than M
# s = 0; k = 1; M = 100
# while k < M:
#     s += 1/k
# print(s)

s = 0
k = 1
M = 100
while s < M:
    s += 1/k
print(s)
