q = [r**2 for r in [10**i for i in xrange(5)]]

# Convert nested list comprehensions to nested standard loops
q2 = list()
for i2 in xrange(5):
    for r in [10**i2]:
        q2.append(r**2)

assert q == q2
