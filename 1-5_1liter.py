from __future__ import division, print_function


def mass_of_one_liter(txt):
    """ Converts a given density from cubic centimeters to the
        mass of one liter of given substance.
        Return the name of substance and mass in grams. """
    mass = txt.rstrip().split()
    return ' '.join(mass[:-1]), 1000 * float(mass[-1])

with open('src\\files\\densities.dat', 'r') as f:
    for line in f:
        substance, g = mass_of_one_liter(line)
        print('1 liter of {} weighs {}g | {}kg'.format(substance, g, g / 1000))
