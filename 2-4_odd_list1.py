from __future__ import division, print_function


def odd_while(limit):
    """ Exercise was to use a while loop and save to a list """
    odds = list()
    cnt = 1
    while cnt < limit:
        if not cnt % 2 == 0:
            odds.append(cnt)
        cnt += 1
    return odds

def odd_range(limit):
    return range(1, limit+1, 2)

# after list saved, print the list elements
w_odd = odd_while(100)
[print(odd) for odd in w_odd]

# or using range...
r_odd = odd_range(100)
[print(b) for b in r_odd]
