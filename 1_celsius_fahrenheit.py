from __future__ import division, print_function


def celsius_to_fahrenheit(celsius):
    return (9 / 5) * celsius + 32


def fahrenheit_to_celsius(fahrenheit):
    return (5 / 9) * (fahrenheit - 32)

print(celsius_to_fahrenheit(21.0))  # 69.8
print(fahrenheit_to_celsius(69.8))  # 21.0
