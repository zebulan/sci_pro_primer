from __future__ import division, print_function

def seconds_to_years(seconds):
    return seconds / 31536000

print(seconds_to_years(1000000000))
