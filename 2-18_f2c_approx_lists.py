from __future__ import division, print_function


def approx_celsius(fahrenheit):
    """ Return approximate celsius from fahrenheit """
    return (fahrenheit - 30) / 2


def exact_celsius(fahrenheit):
    """ Return exact celsius from fahrenheit """
    return (5 / 9) * (fahrenheit - 32)


def three_lists(limit):
    fahrenheit = list()
    celsius = list()
    approx_c = list()
    for a in xrange(0, limit+1, 10):
        fahrenheit.append(a)
        celsius.append(exact_celsius(a))
        approx_c.append(approx_celsius(a))
    return fahrenheit, celsius, approx_c


def table_print(limit):
    for f, c, c_approx in zip(*three_lists(limit)):
        print('F: {:6.2f}\tC: {:6.2f}\tC-Approx: {:6.2f}'.
              format(f, c, c_approx))

table_print(100)
