from __future__ import division, print_function
from math import pi, sqrt


def gaussian(m, s, x):
    return (1 / (sqrt(2 * pi) * s))**(-0.5*(x - m / s ** 2))

print(gaussian(0, 2, 1))  # 2.23903026984
# not exactly sure this answer is correct...
