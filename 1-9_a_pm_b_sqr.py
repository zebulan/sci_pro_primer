from __future__ import division, print_function


def a_plus_minus_sqr(a, b):
    a2 = a**2
    b2 = b**2
    eq1_sum = a2 + 2*a*b + b2
    eq2_sum = a2 - 2*a*b + b2
    eq1_pow = (a + b)**2
    eq2_pow = (a - b)**2
    print('First equation:  {:g} = {:g}'.format(eq1_sum, eq1_pow))
    print('Second equation: {} = {}'.format(eq2_pow, eq2_sum))

a_plus_minus_sqr(3, 5)
a_plus_minus_sqr(3, 3)

# # a = 3  # 64 = 64, 4 = 4
# # b = 5
# a = 3  # 36 = 36, 0 = 0
# b = 3
#
# a2 = a**2
# b2 = b**2
#
# eq1_sum = a2 + 2*a*b + b2
# eq2_sum = a2 - 2*a*b + b2
#
# eq1_pow = (a + b)**2
# eq2_pow = (a - b)**2
#
# print('First equation:  {:g} = {:g}'.format(eq1_sum, eq1_pow))
# print('Second equation: {} = {}'.format(eq2_pow, eq2_sum))
