from __future__ import division, print_function

# # rewrite below as a for loop
# s = 0
# k = 1
# M = 100
# while s < M:
#     s += 1/k
# print(s)

k = 1
M = 100
print(sum(1/k for a in xrange(k, M+1)))
