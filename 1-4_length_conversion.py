from __future__ import division, print_function

def convert_from_meters(meters):
    """ Returns four variables, inches, feet, yards, miles """
    return meters * 39.3701, meters * 3.28084,\
           meters * 1.09361, meters * 0.000621371

inch, feet, yard, mile = convert_from_meters(640)
print('{:.2f} inch(es)'.format(inch))  # 25196.86
print('{:.2f} ft'.format(feet))        # 2099.74
print('{:.2f} yard(s)'.format(yard))   # 699.91
print('{:.4f} mile(s)'.format(mile))   # 0.3977
