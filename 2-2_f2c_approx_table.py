from __future__ import division, print_function


def approx(fahrenheit):
    """ Return approximate celsius from fahrenheit """
    return (fahrenheit - 30) / 2


def exact(fahrenheit):
    """ Return exact celsius from fahrenheit """
    return (5 / 9) * (fahrenheit - 32)


def conversion_table(limit):
    for a in range(0, limit+1, 10):
        print('Fahrenheit: {}\t\tCelsius: {:.2f}\t\tApprox: {:.2f}'.
              format(a, exact(a), approx(a)))

conversion_table(300)
