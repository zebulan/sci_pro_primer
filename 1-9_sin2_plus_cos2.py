from __future__ import division
from math import cos, pi, sin

x = pi / 4
value = sin(x)**2 + cos(x)**2
assert value == 1.0

# Does sin^2(x) + cos^2 = 1? -> Yes.
# had to add pi to the import statement, refactor the formula
# and change the variable name so it didn't start with 1.
