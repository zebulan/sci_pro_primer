from __future__ import print_function


def height_of_ball(initial_velocity, acceleration_of_gravity, time):
    """ Returns the height in meters """
    position = initial_velocity * time - 0.5 *\
               acceleration_of_gravity * time ** 2
    return time, position

time, position = height_of_ball(5, 9.81, 0.6)
assert position == 1.2342

print('At t={:g} s, the height of the ball is {:.2f} m.'.format(time, position))
