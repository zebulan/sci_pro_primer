from __future__ import print_function
from math import sin

# NameError: name 'sin' is not defined
# missing import from math
# x = 1; print('sin(%g) = %g' % (x, sin(x)))

print('sin({:g}) = {:g}'.format(1, sin(1)))
