from __future__ import division, print_function
from math import pi, sqrt


def km_h_to_km_s(km):
    return km * 1000 / 3600

def drag_force(drag_co, density, area, velocity):
    return 0.5 * drag_co * density * area * velocity ** 2


def gravity_force(mass):
    return mass * 9.81 ** -2

Q = 1.2 ** -3  # kg / m ** -3  # density of air
a = 11  # cm (football)
A = pi * a ** 2  # cross-sectional area (normal to the velocity direction)
m = 0.43  # kg (football)  # mass
Cd = 0.2  # drag coefficient

print('{:.1f}'.format(drag_force(Cd, Q, A, km_h_to_km_s(120))))
print('{:.1f}'.format(drag_force(Cd, Q, A, km_h_to_km_s(10))))
print(gravity_force(m))
