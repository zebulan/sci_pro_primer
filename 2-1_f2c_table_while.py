from __future__ import division, print_function


def f_to_c(fahrenheit):
    return (5 / 9) * (fahrenheit - 32)


def conversion_table(limit):
    for a in range(0, limit+1, 10):
        print('Fahrenheit: {}\t\tCelsius: {:.2f}'.format(a, f_to_c(a)))

conversion_table(300)
