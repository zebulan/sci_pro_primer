from __future__ import division, print_function


def odd_while(limit):
    """ Exercise was to use a while loop """
    cnt = 1
    while cnt < limit:
        if not cnt % 2 == 0:
            yield cnt
        cnt += 1


def odd_xrange(limit):
    for a in xrange(1, limit+1, 2):
        yield a
