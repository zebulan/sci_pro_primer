from __future__ import division, print_function
from math import pi


def parallelogram(height, base):
    """ Returns the area of a parallelogram """
    return height * base


def square(base):
    """ Returns the area of a square """
    return base ** 2


def circle(radius):
    """ Returns the area of a circle """
    return pi * radius ** 2


def cone(height, radius):
    """ Returns the volume of a cone """
    return 1 / 3 * pi * radius ** 2 * height

h = 5.0  # height
b = 2.0  # base
r = 1.5  # radius

print('The area of the parallelogram is {:.3f}'.format(parallelogram(h, b)))
print('The area of the square is {:g}'.format(square(b)))
print('The area of the circle is {:.3f}'.format(circle(r)))
print('The volume of the cone is {:.3f}'.format(cone(h, r)))


# # Below is copied from the book, as per exercise instructions.
# # Only thing I changed was the old % formatting to the format function.
# # I rewrote the calculations into functions instead.
# h = 5.0
# b = 2.0
# r = 1.5
#
# area_parallelogram = h * b
# print('The area of the parallelogram is {:.3f}'.format(area_parallelogram))
#
# area_square = b ** 2
# print('The area of the square is {:g}'.format(area_square))
#
# area_circle = pi * r ** 2
# print('The area of the circle is {:.3f}'.format(area_circle))
#
# volume_cone = 1 / 3 * pi * r ** 2 * h
# print('The volume of the cone is {:.3f}'.format(volume_cone))
