from __future__ import division, print_function


def money_growth(initial, interest_rate, years):
    """ Return the total amount of money, with interest """
    return round(initial * (1 + interest_rate / 100) ** years, 2)

assert money_growth(1000, 5, 3) == 1157.63
