from __future__ import division, print_function

year_sun_hours = list()
with open('src\\misc\\Oxford_sun_hours.txt', 'r') as f:
    for line in f:
        tmp = list()
        for a in line.rstrip().strip(',[]').split(','):
            current = a.strip()
            if current:
                tmp.append(float(current))
        if tmp:
            year_sun_hours.append(tmp)

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
          'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

averages = list()
for month, hours in zip(months, zip(*year_sun_hours)):
    averages.append((sum(hours) / len(hours), month))

# 1 - Compute the average number of sun hours for each month in total data
for hours, month in averages:
    # print('{}\'s average sun hours = {:>3.3f}'.format(month, hours))
    print('{}: {:.2f}'.format(month, hours))

# 2 - Which month has the best weather according to the means above?
b_hr, b_mon = max(averages)
print('\n{}: {:.2f} - highest average sun hours\n'.format(b_mon, b_hr))

# 3 - For each decade, 1930-1939, 1940-1949, ..., 2000-2009, compute
# the average number of sun hours per day in January AND December.
# For example, use Dec 1949, Jan 1950,..., Dec 1958, Jan 1959 as the
# data for the decade 1950 - 1959.
# Are there any noticeable differences between the decades?
decades = dict()
tmp = list()
for year, hours in zip(xrange(1929, 2010), year_sun_hours):
    if str(year)[-1] == '9':   # last/first of decade
        tmp.append(hours[0])   # January
        if not len(tmp) == 1:  # skip first January
            decades[year-9] = sum(tmp) / (20 * 30)
        tmp = list()
        tmp.append(hours[11])  # December (first of new decade)
    else:
        tmp.append(hours[0])   # January
        tmp.append(hours[11])  # December

for k in sorted(decades):
    print('Decade {}-{}: {:.2f}'.format(k, k+9, decades[k]))
