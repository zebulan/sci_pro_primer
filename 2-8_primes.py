from __future__ import print_function


def print_em(lst):
    [print(a) for a in lst]

primes = [2, 3, 5, 7, 11, 13]
print_em(primes)

p = 17
primes.append(p)
print_em(primes)
