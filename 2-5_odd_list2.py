from __future__ import division, print_function


def odd_list_comp(limit):
    return [a for a in xrange(1, limit+1, 2)]

[print(b) for b in odd_list_comp(100)]
